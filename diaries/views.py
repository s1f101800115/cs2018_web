from django.shortcuts import render, redirect
from diaries.models import Diary, User

# Create your views here.
def top(request):
    return render(request, 'diaries/top.html')

def index(request):
    diaries = Diary.objects.all()
    data_dictionary = {'diaries': diaries}
    return render(request, 'diaries/index.html', data_dictionary)

def detail(request, diary_id):
    diary = Diary.objects.get(pk=diary_id)
    data_dictionary = {'diary': diary}
    return render(request, 'diaries/detail.html', data_dictionary)

def create(request):
    if request.method == 'POST':
        diary = Diary.objects.create(
            title = request.POST['title'],
            detail = request.POST['detail'],
            created = request.POST['created'],
            user_id = request.POST['user_id']
        )
        return redirect('detail', diary.id)
    users = User.objects.all()
    data = {
        'users': users
    }
    return render(request, 'diaries/create.html', data)

def update(request, diary_id):
    diary = Diary.objects.get(pk=diary_id)
    if request.method == "POST":
        diary.title = request.POST.get('title')
        diary.detail = request.POST.get('detail')
        diary.created = request.POST.get('created')
        diary.user_id = request.POST.get('user_id')
        diary.save()
        return redirect('/diaries/'+str(diary.id))
    users = User.objects.all()
    data = {
        'diary': diary,
        'users': users
    }
    return render(request, 'diaries/update.html', data)
