from django.db import models
import datetime
# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class Diary(models.Model):
    title = models.CharField(max_length=256)
    detail = models.TextField(default="This is a diary.")
    created = models.DateTimeField(default=datetime.datetime.now)
    user = models.ForeignKey(User, related_name='diaries', on_delete=models.CASCADE)
